(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

open Cil_types
open Meta_utils
open Meta_parse
open Meta_options

type unpacked_metaproperty = {
  ump_emitter : Emitter.t;
  ump_property : Kernel_function.t -> ?stmt:stmt -> (string * replaced_kind) list -> predicate;
  ump_ip : Property.t;
  ump_counter : int ref;
  ump_admit : bool;
  ump_assert : bool;
}

val dispatch : meta_flags ->
  metaproperty list ->
  (context * string list Str_Hashtbl.t) list *
  (string, unpacked_metaproperty) Hashtbl.t

val name_mp_pred : meta_flags ->
  predicate -> int ref -> predicate

val add_dependency : unpacked_metaproperty -> Property.t list -> unit
val finalize_dependencies : unit -> unit
