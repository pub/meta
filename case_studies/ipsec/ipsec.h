#include <stddef.h>

/*
enum protocol { PROT_IPSEC, PROT_CLEAR }; //IPSEC : AH or ESP; CLEAR : any other

struct ip_datagram {
    int src_ip, dst_ip;
    enum protocol prot;
    char* data;
    size_t data_len;
};

struct ipsec_datagram { //Simplification of AH or ESP
    long spi;
    struct ip_datagram inner;
};

struct sad_entry {
    //Key
    int dest_ip;
    long spi;

    //Value
    int enc_type;
    long enc_key;
};
*/

struct Input {
    char* data;
    size_t length;
    long enc_key;
    int enc_type;
};

struct Input* inputs;
size_t input_nb;

/*@ meta \prop, \name(inputs_well_formed),
            \targets(\ALL), \context(\strong_invariant),
            \valid_read(inputs + (0 .. (input_nb - 1)))
            && \forall size_t i; 0 <= i < input_nb ==>
            \valid(inputs[i].data + (0 .. (inputs[i].length - 1)));
*/

/*@ axiomatic encryption {
        predicate encrypted{L1, L2}(char* src, size_t slen,
                                    char* dst, size_t dlen,
                                    integer enc_type, long enc_key)
                                    reads \at(src[0 .. (slen - 1)], L1),
                                          \at(dst[0 .. (dlen - 1)], L2);
    }
*/

/* predicate same_block{L1, L2}(char* b1, size_t l1, char* b2, size_t l2) =
        l1 == l2 &&
        \forall size_t i; 0 <= i < l1 ==> \at(b1[i], L1) == \at(b2[i], L2);

        //If b1 = b2 and b3 is the encryption of b2 then it is also the
        //encryption of b1
        axiom encrypted_left{L1, L2, L3}:
            \forall integer enc_type, long enc_key,
            char *b1, *b2, *b3, size_t l1, l2, l3;
                \valid{L1}(b1 + (0 .. (l1 - 1))) && \valid{L2}(b2 + (0 .. (l2 - 1))) &&
                \valid{L3}(b3 + (0 .. (l3 - 1))) && same_block{L1, L2}(b1, l1, b2, l2) &&
                encrypted{L2, L3}(b2, l2, b3, l3, enc_type, enc_key)
                ==> encrypted{L1, L3}(b1, l1, b3, l3, enc_type, enc_key);

        //If b2 is the encryption of b1 and b3 = b2 then it is also the
        //encryption of b1
        axiom encrypted_right{L1, L2, L3}:
            \forall integer enc_type, long enc_key,
            char *b1, *b2, *b3, size_t l1, l2, l3;
                \valid{L1}(b1 + (0 .. (l1 - 1))) && \valid{L2}(b2 + (0 .. (l2 - 1))) &&
                \valid{L3}(b3 + (0 .. (l3 - 1))) && same_block{L2, L3}(b2, l2, b3, l3) &&
                encrypted{L1, L2}(b1, l1, b2, l2, enc_type, enc_key)
                ==> encrypted{L1, L3}(b1, l1, b3, l3, enc_type, enc_key);
}
*/

/*@
    requires \valid(dat + (0 .. (len - 1)));
    requires \valid(flen);
    ensures \valid(\result + (0 .. (\at(*flen, Post))));
    ensures encrypted{Pre, Post}(dat, len, \result, \at(*flen, Post), 1, key);
    ensures \separated(dat + (0 .. (len - 1)), \result + (0 .. (\at(*flen, Post) - 1)));
    assigns *flen;
*/
char* encrypt_1(char* dat, size_t len, size_t* flen, long key);

/*@
    requires \valid(dat + (0 .. (len - 1)));
    requires \valid(flen);
    ensures \valid(\result + (0 .. (\at(*flen, Post))));
    ensures encrypted{Pre, Post}(dat, len, \result, \at(*flen, Post), 2, key);
    ensures \separated(dat + (0 .. (len - 1)), \result + (0 .. (\at(*flen, Post) - 1)));
    assigns *flen;
*/
char* encrypt_2(char* dat, size_t len, size_t* flen, long key);

/*@ requires \exists integer i; 0 <= i < input_nb &&
                \let entry = inputs + i;
                encrypted{Here, Here}(entry->data, entry->length,
                                      data, len, entry->enc_type,
                                      entry->enc_key);
    assigns \nothing;
*/
void insecure_send(char* data, size_t len);
