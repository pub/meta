#include "ipsec.h"
#include <string.h>

/*@ requires i < input_nb; */
void encrypt_and_send(size_t i) {
    size_t rs;
    char* enc;
    if(inputs[i].enc_type == 1) {
        enc = encrypt_1(inputs[i].data, inputs[i].length,
                &rs, inputs[i].enc_key);
    }
    else if(inputs[i].enc_type == 2) {
        enc = encrypt_2(inputs[i].data, inputs[i].length,
                &rs, inputs[i].enc_key);
    }
    else return;
    insecure_send(enc, rs);
}
