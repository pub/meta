This directory only contains MetACSL specifications and specific files
for driving the analysis of wookey's bootloader.

To get the wookey source themselves, and configure them so that they are
ready for analysis, just run `setup.sh` in a repo with no uncommitted change.
`setup.sh` depends on the software below.

Once setup is done, go to the `loader` directory and do `make loader.meta`
(assuming you have a working Frama-C installation with MetAcsl installed,
as well as the proprietary ACSL-Importer plug-in).

## Dependencies:
- repo
- bear
- kconfig-conf (from the kconfig-frontends package)
- arm-none-eabi-gcc
