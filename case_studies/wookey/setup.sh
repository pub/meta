SCRIPT_DIR=`pwd`
read -p "!!!! PRESS ENTER ONLY IF ALL CHANGES IN THE META REPO ARE COMMITTED !!!!"
if ! [ -x "$(command -v repo)" ]; then
    echo "ERROR: the 'repo' program is not installed" >&2
    exit 1
fi
if ! [ -x "$(command -v bear)" ]; then
    echo "ERROR: the 'bear' program is not installed" >&2
    exit 1
fi
if ! [ -x "$(command -v kconfig-conf)" ]; then
    echo "ERROR: the 'kconfig-conf' program is not installed" >&2
    exit 1
fi
repo init -u https://github.com/wookey-project/manifest.git -m wookey_stable.xml
echo "Cloning..."
repo sync --force-sync > /dev/null
cd $SCRIPT_DIR/.. && git reset --hard
rm -rf $SCRIPT_DIR/.git
rm -rf $SCRIPT_DIR/loader/.git
rm -rf $SCRIPT_DIR/.gitignore
echo "Done cloning !"
. $SCRIPT_DIR/setenv.sh
cd $SCRIPT_DIR && make boards/wookey/configs/wookey_production_defconfig
cd $SCRIPT_DIR/loader && bear make -f Makefile 2> /dev/null
echo "compile_commands.json generated"
