#include "automaton.h"

/////////////////////////////////////////////////////////
//   _____        __ _       _ _   _                   //
//  |  __ \      / _(_)     (_) | (_)                  //
//  | |  | | ___| |_ _ _ __  _| |_ _  ___  _ __  ___   //
//  | |  | |/ _ \  _| | '_ \| | __| |/ _ \| '_ \/ __|  //
//  | |__| |  __/ | | | | | | | |_| | (_) | | | \__ \  //
//  |_____/ \___|_| |_|_| |_|_|\__|_|\___/|_| |_|___/  //
//                                                     //
/////////////////////////////////////////////////////////

// Macros

#define UNTOUCHED(title, tset, loc) \
    meta \prop, \
    \name(title), \
    \targets(tset), \
    \context(\writing), \
    \separated(\written, loc);

#define DEDUCED_NEG_ASSIGNS(title, tset, val) \
    meta \prop, \
    \name(title), \
    \targets(tset), \
    \context(\postcond), \
    \flags(proof:deduce, translate:yes), \
    val == \old(val);

// Function sets

#define ALL_EXCEPT_SELECTBANK (\diff(\ALL, loader_exec_req_selectbank))
#define CALLED_BY_SELECTBANK (\diff(\callees(loader_exec_req_selectbank), loader_exec_req_selectbank))
#define TRANSITION_FUNCTIONS ({         \
    loader_exec_req_init,              \
    loader_exec_req_rdpcheck,          \
    loader_exec_req_dfucheck,          \
    loader_exec_req_selectbank,        \
    loader_exec_req_crccheck,          \
    loader_exec_req_integritycheck,    \
    loader_exec_req_flashlock,         \
    loader_exec_error,                 \
    loader_exec_secbreach,             \
    loader_exec_req_boot               \
})

// Predicates

#define VALID_LOGIC_STATE (\exists integer i; is_cell_accessor_index(i, logic_state))

////////////////////////////////////////////////
//  ______ _ _              __ _              // 
// |  ____| (_)            / _| |             //
// | |__  | |_ _ __ ______| |_| | ___  _ __   //
// |  __| | | | '_ \______|  _| |/ _ \| '_ \  //
// | |    | | | |_) |     | | | | (_) | |_) | //
// |_|    |_|_| .__/      |_| |_|\___/| .__/  //
//            | |                     | |     //
//            |_|                     |_|     //
////////////////////////////////////////////////


/*@ UNTOUCHED(bank_context_ro_1, ALL_EXCEPT_SELECTBANK, &ctx.fw) */
/*@ UNTOUCHED(bank_context_ro_2, ALL_EXCEPT_SELECTBANK, &ctx.boot_flip) */
/*@ UNTOUCHED(bank_context_ro_3, ALL_EXCEPT_SELECTBANK, &ctx.boot_flop) */

// To prove correction of req_selectbank

/*@ UNTOUCHED(ctxbflip_not_written_by_utils, CALLED_BY_SELECTBANK, &ctx.boot_flip) */
/*@ DEDUCED_NEG_ASSIGNS(ctxbflip_not_changed_by_utils, CALLED_BY_SELECTBANK, ctx.boot_flip) */

/*@ UNTOUCHED(ctxbflop_not_written_by_utils, CALLED_BY_SELECTBANK, &ctx.boot_flop) */
/*@ DEDUCED_NEG_ASSIGNS(ctxbflop_not_changed_by_utils, CALLED_BY_SELECTBANK, ctx.boot_flop) */

/*@ UNTOUCHED(ctxfw_not_written_by_utils, CALLED_BY_SELECTBANK, &ctx.fw) */
/*@ DEDUCED_NEG_ASSIGNS(ctxfw_not_changed_by_utils, CALLED_BY_SELECTBANK, ctx.fw) */

/*@ meta \prop,
        \name(selectbank_called_once),
        \targets(loader_exec_req_selectbank),
        \context(\precond),
        \flags(proof:axiom),
        ctx.fw == 0;
*/


/////////////////////////////////////////////////////////////
//                _                        _               //
//     /\        | |                      | |              //
//    /  \  _   _| |_ ___  _ __ ___   __ _| |_ ___  _ __   //
//   / /\ \| | | | __/ _ \| '_ ` _ \ / _` | __/ _ \| '_ \  //
//  / ____ \ |_| | || (_) | | | | | | (_| | || (_) | | | | //
// /_/    \_\__,_|\__\___/|_| |_| |_|\__,_|\__\___/|_| |_| //
//                                                         //
/////////////////////////////////////////////////////////////


//############ RESTRICT STATE CHANGING #############

/*@ meta \prop,
        \name(transitions_honor_next_state),
        \targets(\diff(TRANSITION_FUNCTIONS,
                    \union(loader_exec_req_boot,
                           loader_exec_error,
                           loader_exec_secbreach))),
        \context(\postcond),
        \fguard(
            logic_state == \formal(nextstate) ||
            logic_state == LOADER_ERROR
        );
*/


//=========================================================
//== State only changed in set_state,
//== constant value otherwise
//==

/*@ UNTOUCHED(state_only_changed_in_wrapper,
              \diff(\ALL, loader_set_state),
              &logic_state) */


/*@ meta \prop,
        \name(no_change_of_state),
        \targets(\diff(\ALL,
                    \union(loader_set_state,
                        loader_is_valid_transition,
                        \callers(TRANSITION_FUNCTIONS)))),
        \context(\postcond),
        \flags(proof:deduce),
        logic_state == \old(logic_state);
*/

//==
//==
//=========================================================

//=========================================================
//== Logic state and real state are equivalent
//==

/*@ UNTOUCHED(real_state_not_touched, loader_get_state, &state) */

/*@ meta \prop,
        \name(state_glue_small),
        \targets(loader_set_state),
        \context(\postcond),
        state == logic_state;
*/

/*@ meta \prop,
        \name(state_glue),
        \targets({loader_set_state, loader_get_state}),
        \context(\weak_invariant),
        \flags(proof:deduce, translate:yes),
        state == logic_state;
*/

//==
//==
//=========================================================

//=========================================================
//== The current state is always valid (exists in struct)
//==

/*@ meta \prop,
        \name(state_always_valid_small),
        \targets(loader_set_state),
        \context(\strong_invariant),
        \flags(proof:axiom, translate:no),
        VALID_LOGIC_STATE;
*/

/*@ meta \prop,
        \name(state_always_valid_callees),
        \targets(\callees(loader_set_state)),
        \context(\strong_invariant),
        \flags(proof:deduce, translate:no),
        VALID_LOGIC_STATE;
*/

/*@ meta \prop,
        \name(state_always_valid),
        \targets(\ALL),
        \context(\weak_invariant),
        \flags(proof:deduce, translate:yes),
        VALID_LOGIC_STATE;
*/

//==
//==
//=========================================================

/*@ meta \prop,
        \name(state_wrapper_only_called_in_transitions),
        \targets(\diff(\ALL,
                    \union(TRANSITION_FUNCTIONS,
                           main,
                           loader_exec_automaton_transition,
                           loader_exec_req_boot,
                           loader_is_valid_transition,
                           loader_set_state))),
        //Would be better to ensure that for those exceptions,
        //it is only called to set an error state
        //Need for a mechanism to TO that
        \context(\calling),
        \tguard(\called != loader_set_state);
*/


//############ RESTRICT TRANSITIONS ################

/*@
    meta \prop,
        \name(transitions_called_from_authorized_sources),
        \targets(\diff(\ALL, {loader_exec_automaton_transition})),
        \context(\calling),
            \tguard( \called != loader_exec_req_init ) &&
            \tguard( \called != loader_exec_req_rdpcheck ) &&
            \tguard( \called != loader_exec_req_dfucheck ) &&
            \tguard( \called != loader_exec_req_selectbank ) &&
            \tguard( \called != loader_exec_req_crccheck ) &&
            \tguard( \called != loader_exec_req_integritycheck ) &&
            \tguard( \called != loader_exec_req_boot ) &&
            \tguard( \called != loader_exec_secbreach ) &&
            \tguard( \called != loader_exec_req_flashlock );
*/

/*@
    meta \prop,
        \name(automaton_transition_executed_from_automaton),
        \targets(\diff(\ALL, loader_exec_automaton)),
        \context(\calling),
            \tguard( \called != loader_exec_automaton_transition );
*/

/*@
    meta \prop,
        \name(automaton_executed_from_main),
        \targets(\diff(\ALL, main)),
        \context(\calling),
            \tguard( \called != loader_exec_automaton );
*/

