#ifndef WOW
#define WOW
#define MAX_ROOM_NB 100
#define MAX_LOCK_NB 10
#define MAX_WINDOW_NB 10
#define USER_NB 100

//==================== SPECIFICATION ===================

/*@
	//A room structure is valid iff it is stored in the global array
	//Every API function requires its argument to be valid in that respect
	predicate valid_room(struct Room* r) =
		\exists unsigned i; 0 <= i < cur_room_nb && r == rooms + i;
*/


/*@ meta \macro,
		\name(forall_room),
		\arg_nb(2),
		\forall unsigned ri; 0 <= ri < cur_room_nb ==>
		\let \param_1 = rooms + ri; \param_2;
	meta \macro,
		\name(\constant),
		\arg_nb(1),
		\separated(\written, \param_1);
*/

#define USER_SET (\callees(receive_command))

/*@ meta \prop,
		\name(valid_room),
		\targets(USER_SET),
		\context(\precond),
		\tguard(valid_room(\formal(room)));
*/

/*@ meta \prop,
		\name(only_room_unlock_unlocks),
		\targets(\diff(USER_SET, room_unlock)),
		\context(\writing),
	forall_room(r,
		\at(r->door_lock_state, Before) != 0 //Iif 
		&& \at(r->door_lock_state, After) == 0
		==> \constant(&r->door_lock_state)
	);
*/

/*@ meta \prop,
		\name(unlocking_needs_permission_or_alarm),
		\targets(USER_SET),
		\context(\writing),
	forall_room(r,
		\at(r->door_lock_state, Before) != 0
		&& \at(r->door_lock_state, After) == 0
		&& \fguard(user_permissions[\formal(uid)] < r->clearance_needed)
		&& alarm_status != ALARM_UNLOCKING
		==> \constant(&r->door_lock_state)
	);
*/

/*@ meta \prop,
		\name(alarm_not_disabled_by_user),
		\targets(USER_SET),
		\context(\writing),
	\at(alarm_status, Before) != AC_DISABLED
	&& \at(alarm_status, After) == AC_DISABLED
	==> \constant(&alarm_status);
*/

/*@ meta \prop,
		\name(permissions_constant),
		\targets(USER_SET),
		\context(\writing),
		\constant(user_permissions + (0 .. USER_NB));
*/

/*@ meta \prop,
		\name(no_door_closed_when_alarm),
		\targets(USER_SET),
		\context(\strong_invariant),
	alarm_status == ALARM_RINGING ==>
	forall_room(r, r->door_lock_state == 0);
*/

/*@ meta \prop,
		\name(no_ac_if_window_open),
		\targets(USER_SET),
		\context(\strong_invariant),
	forall_room(r,
		r->window_state == 0
		==> r->ac_state == AC_DISABLED
	);
*/
#endif
