BEGIN {
	FS=","
	OFS=","
}
{
	mf = gensub(/(.*)\.c/,"\\1.monitor", "g", $1)
	content="OK"
	cmd = "sed -n '3p' \""mf"\""
	cmd | getline content
	close(cmd)
	print $1,$2,content
}
