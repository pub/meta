(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

(** use meta-deduction to prove an HILARE *)

(** computes the set of function names that compose the given target. *)
val compute_target: Meta_parse.target -> Meta_utils.StrSet.t

(** [deduce flags mp ip mps] attempts to prove [mp] under the hypothesis that
    [mps] hold, in the environment [flags]. [ip] is the corresponding identified
    property for the kernel: its status will be set according to the result of
    the proof attempt. Returns [true] iff the proof has succeeded.
*)
val deduce:
  Meta_options.meta_flags ->
  Meta_parse.metaproperty ->
  Property.t ->
  Meta_parse.metaproperty list ->
  bool
