#!/bin/bash
##########################################################################
#                                                                        #
#  This file is part of the Frama-C's MetACSL plug-in.                   #
#                                                                        #
#  Copyright (C) 2018-2025                                               #
#    CEA (Commissariat à l'énergie atomique et aux énergies              #
#         alternatives)                                                  #
#                                                                        #
#  you can redistribute it and/or modify it under the terms of the GNU   #
#  Lesser General Public License as published by the Free Software       #
#  Foundation, version 2.1.                                              #
#                                                                        #
#  It is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU Lesser General Public License for more details.                   #
#                                                                        #
#  See the GNU Lesser General Public License version 2.1                 #
#  for more details (enclosed in the file LICENSE)                       #
#                                                                        #
##########################################################################

set -e

# Make a release of MetAcsl. This script is supposed to be launched on the root
# of a clean clone of the git repository,
# with a clone of FC's website as subdirectory

# In order for the gitlab and github related command to succeed, you will need
# to position GITLAB_TOKEN and GITHUB_TOKEN accordingly

# Check everything is alright.

if test -z "$GITLAB_TOKEN" -o -z "$GITHUB_TOKEN"; then
    echo "Please define GITLAB_TOKEN and GITHUB_TOKEN to appropriate values"
    exit 2;
fi

if test ! -d website/.git; then
    echo "website does not exist or is not a git repo"
    exit 2;
fi

PLUGIN=metacsl
PLUGIN_FULL_NAME=MetAcsl
REPO=meta

BRANCH=$(git branch --show-current)

OPAM_BASE=$(basename "$(ls ./*.opam)" .opam)

VERSION=$(grep -e '^version:' "$OPAM_BASE.opam" | sed -e 's/.*"\(.*\)"/\1/')

if test -n "$(git status --untracked=no -s --porcelain)"; then
    echo "Current repo is not clean. Commit your changes before releasing."
    exit 2;
fi

user_validation()
{
    msg="$1"
    echo "$msg [yes/no]?"
    read -r cont
    if test "$cont" != "yes"; then
        echo "Exiting"
        exit 1;
    fi;
}

if grep -q -e '[~+]' <<<"$VERSION"; then
    STABLE=no;
else
    STABLE=yes;
    if ! grep -q -e '^stable/.*' <<< "$BRANCH"; then
        echo "WARNING: you're planning to do a stable release on top of non-stable branch $BRANCH"
        user_validation "Are you sure you want to continue";
    else
        FRAMAC_VERSION_NAME=${BRANCH#stable/}
    fi;
fi

FRAMAC_VERSION_CONSTRAINT=$(grep -e '^ *(frama-c' dune-project)

# shellcheck disable=SC2001
FRAMAC_MIN_VERSION=$(sed -e 's/[^0-9]*\([0-9.]*\).*/\1/' <<< "$FRAMAC_VERSION_CONSTRAINT")

echo "Current branch is $BRANCH"
echo "Targeted version is $VERSION"
echo "Opam package is $OPAM_BASE"
echo "Constraints on Frama-C version are: $FRAMAC_VERSION_CONSTRAINT"
user_validation "Are the values above correct"

if test $STABLE = "yes"; then
    if ! grep -q -e "^#.* $VERSION.*" CHANGELOG.md; then
       echo "WARNING: no entry in CHANGELOG.md for $VERSION"
       user_validation "Do you want to continue";
    fi;
fi

CURRENT_CHANGELOG=$(sed -n -e "/^# *Version *$VERSION/,/^# *Version/p" CHANGELOG.md | head -n -1 | tail -n +2)

echo "Proposed Changelog for release is"
echo "$CURRENT_CHANGELOG"
user_validation "OK"

# Check that we can do an opam install from the repo itself
opam switch create "test-$PLUGIN-$VERSION" ocaml-base-compiler.4.14.1 -y
opam install . -y
opam switch remove "test-$PLUGIN-$VERSION" -y

# push branch on pub

GITLAB_ROOT="https://git-token:$GITLAB_TOKEN@git.frama-c.com"

git push "$GITLAB_ROOT/pub/$REPO" "$BRANCH:$BRANCH"

if test -n "$(git tag -l "$VERSION")"; then
    user_validation "Tag $VERSION already exists. Override"
    TAG_FORCE="-f";
fi

# shellcheck disable=SC2086
git tag $TAG_FORCE "$VERSION" -m "Release $VERSION"

# shellcheck disable=SC2086
git push $TAG_FORCE "$GITLAB_ROOT/frama-c/$REPO" "$VERSION"

# shellcheck disable=SC2086
git push $TAG_FORCE "$GITLAB_ROOT/pub/$REPO" "$VERSION"

MD_CHANGELOG="${CURRENT_CHANGELOG//\\/\\\\}"

RELEASE_CONTENT=$(cat <<EOF
{ "name" : "Release v$VERSION",
  "tag_name": "$VERSION",
  "description": "${MD_CHANGELOG//$'\n'/\\n}"
}
EOF
)

RELEASE=$(curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
 https://git.frama-c.com/api/v4/projects/pub%2F$REPO/releases \
 --json "$RELEASE_CONTENT")

RELEASE_URL=$(jq -r '._links.self' <<< "$RELEASE")

RELEASE_ARCHIVE=$(jq -r '.assets.sources[] | select(.format == "tar.bz2") | .url' <<< "$RELEASE")

DATE=$(date +%d-%m-%Y)

# Only stable releases get an announce on website and an opam package
if test $STABLE != "yes"; then
    exit 0;
fi

WEBSITE_BRANCH=release/$PLUGIN-$VERSION

git -C website checkout -b "$WEBSITE_BRANCH"

cat > "website/_events/$PLUGIN-$VERSION.md" <<EOF
---
layout: default
date: $DATE
short_title: $PLUGIN_FULL_NAME v$VERSION
title: $PLUGIN_FULL_NAME v$VERSION for Frama-C $FRAMAC_MIN_VERSION ${FRAMAC_VERSION_NAME^}
link: /fc-plugins/$PLUGIN.html
---

[$PLUGIN_FULL_NAME](/fc-plugins/$PLUGIN.html) [v$VERSION]($RELEASE_URL) is out.

$(cat DESCRIPTION.md)
See its [homepage](/fc-plugins/$PLUGIN.html) for more information.

Main changes in this release include:
$CURRENT_CHANGELOG
EOF

git -C website add "_events/$PLUGIN-$VERSION.md"
git -C website commit -m "$PLUGIN release $VERSION"
git -C website push "$GITLAB_ROOT/pub/pub.frama-c.com" "$WEBSITE_BRANCH:$WEBSITE_BRANCH"

curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
  https://git.frama-c.com/api/v4/projects/pub%2Fpub.frama-c.com/merge_requests \
  --json "$(cat <<EOF
{ "source_branch": "$WEBSITE_BRANCH",
  "target_branch": "master",
  "title": "$PLUGIN Release $VERSION"
}
EOF
)"

github_api ()
{ payload=();
  if test $# -eq 3; then
      payload=(--json "$3");
  fi;
  #shellcheck disable=SC2086
  curl -H "Authorization: Bearer $GITHUB_TOKEN" \
    --request "$1" "https://api.github.com/$2" "${payload[@]}";
}

GITHUB_USER=$(github_api GET user | jq -r '.login')

GITHUB_ROOT=https://$GITHUB_USER:$GITHUB_TOKEN@github.com

OPAM_FILE=$OPAM_BASE/$OPAM_BASE.$VERSION/opam

OPAM_BRANCH=publish-$OPAM_BASE-$VERSION

git clone "$GITHUB_ROOT/ocaml/opam-repository" opam-repository
git -C opam-repository checkout -b "$OPAM_BRANCH"
mkdir -p "opam-repository/packages/$(dirname "$OPAM_FILE")"
TMP_ARCHIVE=$(mktemp $PLUGIN.XXXXX.tar.bz2)
TMP_OPAM_FILE=$(mktemp $OPAM_BASE.$VERSION.opam.XXXX)
curl -o "$TMP_ARCHIVE" "$RELEASE_ARCHIVE"
grep -v '^\(version\|name\):' "$OPAM_BASE.opam" > "$TMP_OPAM_FILE"
cat >> "$TMP_OPAM_FILE" <<EOF
url {
  src: "$RELEASE_ARCHIVE"
  checksum: [
    "md5=$(md5sum "$TMP_ARCHIVE" | cut -d ' ' -f 1)"
    "sha512=$(sha512sum "$TMP_ARCHIVE" | cut -d ' ' -f 1)"
  ]
}
EOF
opam lint --normalise --check-upstream "$TMP_OPAM_FILE" > "opam-repository/packages/$OPAM_FILE"
git -C opam-repository add "packages/$OPAM_FILE"
git -C opam-repository commit -m "new $OPAM_BASE package"
if test $(curl -o /dev/null -w "%{response_code}" "$GITHUB_ROOT/repos/$GITHUB_USER/opam-repository") = "404"; then
# Create the fork if it does not already exists
    github_api POST repos/ocaml/opam-repository/forks '{}';
fi
git -C opam-repository push "$GITHUB_ROOT/$GITHUB_USER/opam-repository" "$OPAM_BRANCH:$OPAM_BRANCH"
PR_CONTENT=$(
cat <<EOF
{ "title": "$OPAM_BASE Version $VERSION",
  "head" : "$GITHUB_USER:$OPAM_BRANCH",
  "base" : "master",
  "maintainer_can_modify" : true
}
EOF
)
github_api POST repos/ocaml/opam-repository/pulls "$PR_CONTENT"
