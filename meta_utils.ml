(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

module StrSet = Datatype.String.Set
module Str_Hashtbl = Datatype.String.Hashtbl
module Stmt_Hashtbl = Cil_datatype.Stmt.Hashtbl
module Fundec_Hashtbl = Cil_datatype.Fundec.Hashtbl
module Fundec_Set = Cil_datatype.Fundec.Set

let find_hash_list find_opt table key =
  match find_opt table key with
    | Some l -> l
    | None -> []

(* In a hashtable mapping key to lists of values, adds v to that list *)
let add_to_hash_list (find_opt, replace) table key v =
  let old_list = find_hash_list find_opt table key in
  replace table key (v :: old_list)
