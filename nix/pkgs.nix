{ frama-c-repo ? builtins.trace "meta: defaulting frama-c-repo to ${toString ../../frama-c}" ../../frama-c }:
let
  ocamlOverlay = oself: osuper: {
    meta = oself.callPackage ./meta.nix {};
  };
  overlay = self: super: {
    ocaml-ng = super.lib.mapAttrs (
      name: value:
        if builtins.hasAttr "overrideScope" value
        then value.overrideScope ocamlOverlay
        else value
    ) super.ocaml-ng;
    release = self.callPackage ./release.nix {
      git = pkgs.git;
      jq = pkgs.jq;
      curl = pkgs.curl;
      git-lfs = pkgs.git-lfs;
      opam = pkgs.opam;
    };
  };
  pkgs = (import (frama-c-repo + "/nix/pkgs.nix")).appendOverlays [ overlay ];
in
pkgs
