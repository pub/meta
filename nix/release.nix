{
  stdenv,
  curl,
  git,
  git-lfs,
  jq,
  opam
}:
stdenv.mkDerivation rec {
  name = "release";
  buildInputs = [
    curl
    git
    git-lfs
    jq
    opam
  ];
}
