{ mk_plugin
, swi-prolog
}:

mk_plugin {
  plugin-name = "metacsl" ;
  plugin-src = fetchGit { shallow=true ; url=./.. ; } ;
  additional-check-inputs = [ swi-prolog ] ;
  has-wp-proofs = true ;
}
