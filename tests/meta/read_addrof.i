/* run.config
OPT: @META@ @PRINT@
*/

unsigned char H[10];

/*@  \meta::meta \prop, \name(len_never_read_unless), \targets(\ALL),
  \context(\reading),
  \separated(\read,H+(0..9) );
*/

int f(){
  int *p;
  int x;

// We're taking the address. no read access
  p=&H[0];

// read access to p and *p
  x=*p;

// read access to H[3];
  p = &H[H[3]];

  // no read access
  x = sizeof(H) + sizeof(H[4]);

  return 0;
}
	
