/* run.config
   OPT: @META@ @PRINT@
*/

void g1(int x, float y);
void g2(int y);
void g3(int x);

void f(int x) {
    g1(0, 42);
    g2(1 + 1);
    g3(x + 2);
}

/*@ \meta::meta \prop,
        \name(p42_not_used_as_y),
        \targets(\ALL),
        \context(\calling),
        \tguard(\called_arg(y) != 42);
*/

/*@ \meta::meta \prop,
        \name(p42_not_used_as_y_for_g2),
        \targets(\ALL),
        \context(\calling),
        \tguard(\called == g2 ==> \fguard(\called_arg(y) != 42));
*/
