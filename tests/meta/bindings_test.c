/* run.config
   OPT: -machdep x86_32 @META@ @PRINT@
*/
#include <stdlib.h>

int* alloc() {
	static int memory[100];
	static int counter = 0;
	int* res = memory + counter++;
	//@ \meta::imeta \bind(res, value);
	return res;
}

void main() {
	int myvar;
	int* i1 = alloc();
	int* i2 = alloc();
	int* i3 = &myvar;
	*i3 = 0;
	i1 = i3;
	*i1 = 0;
	i1 = i2;
	*i1 = 0;
}

/*@ \meta::meta \prop, \name(no_null_dereference), \targets(\ALL), \context(\writing),
				\separated(\bound(value), \written);
*/
