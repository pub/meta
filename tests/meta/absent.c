/* run.config
   OPT: @META@ -cpp-extra-args="-DFULL_CODE" -then-last -print
   OPT: @META@ -meta-warn-key unknown-func=active -then-last -print
*/

int x;

void f() {
  if (x > 100)
    x--;
  else x++;
}

#ifdef FULL_CODE
void g() { f(); }
#endif

/*@ \meta::meta \prop, \name(xpos1), \targets({f,g}), \context(\weak_invariant),
  x >= 0;
*/

/*@ \meta::meta \prop,
  \name(xpos2), \targets(\callees(g)), \context(\strong_invariant),
  x>=0;
*/
