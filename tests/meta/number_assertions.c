/* run.config
   OPT: @META@ -meta-number-assertions @PRINT@
   OPT: @META@ -kernel-verbose 0 -meta-separate-annots @PRINT@
*/

int *p1;
int *p2;

void f1() {
    *p1 = 4;
    *p2 = 4;
    *p1 = 2;
    *p2 = 2;
}

void f2() {
    *p1 = 4;
    *p2 = 4;
    *p1 = 2;
    *p2 = 2;
}

void f3() {
    *p1 = *p2;
    {
        *p2 = *p1;
        {
            p1 = p2;
        }
    }
}

/*@ \meta::meta \prop,
       \name(mp_1),
       \targets(\ALL),
       \context(\writing),
       \separated(\written, p1);
 */


/*@ \meta::meta \prop,
       \name(mp_2),
       \targets(\ALL),
       \context(\reading),
       \separated(\read, p2);
 */

/*@ \meta::meta \prop,
       \name(mp_3),
       \targets(\ALL),
       \context(\strong_invariant),
       *p1 == 0;
 */
