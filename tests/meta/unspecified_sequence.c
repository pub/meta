/* run.config
   OPT: @META@ -meta-no-simpl -meta-no-check-ext @PRINT@
*/

short *p;
short *q;
short S[10];
short H[10];

/*@
  assigns q \from &H[0];
  ensures  \exists integer off; 3 <= off < 8 && q == &H[off];
*/
void get_q(void);

/*@
\meta::meta \prop, \name(ldo_intact), \targets(\ALL), \context(\writing),
  \separated(\written,H+(0..2) );

\meta::meta \prop, \name(ldo_never_read), \targets(\ALL), \context(\reading),
  \separated(\read,H+(0..2) );
*/

/*@
  requires \exists integer off; 3 <= off < 8 && p == &S[off];
  assigns p \from p;
  assigns q \from &H[0];
  assigns p, H[0..9];
*/
void foo(){
  short a;
  a = *(p--);
  get_q();
  //@ assert \exists integer off; 3 <= off < 8 && q == &H[off];
}
