/* run.config
   OPT: @META@ @PRINT@
*/
int x = 0;

/*@
	predicate inv = x == 0;
	predicate diff = x != 4;
*/

void main() {
	int y;
	y = 0;
	x = 4;
	x = y;
}

/*@
 \meta::meta \prop,
		\name(strong_test),
		\targets(main),
		\context(\strong_invariant),
		diff ==> inv;
*/
