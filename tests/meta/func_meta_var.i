/* run.config
   OPT: @META@ -then-last -print
*/
int (*ok)(void);

int x;

int f() {
  if (ok == f)
    x = 42;
  return 0;
}

/*@ \meta::meta \prop, \name(test), \targets(f), \context(\writing),
         !\separated(\written,&x) ==> ok == \func;
*/
