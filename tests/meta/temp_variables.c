/* run.config
   OPT: @META@ @PRINT@
*/
int* G;

int foo(int x) {
	if(x == 0)
		return 1;
	return 2;
}

/*@
 \meta::meta \prop, \name("writing_1"), \targets(foo), \context(\writing),
   \separated(\written, G);
*/
