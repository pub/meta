/* run.config
   OPT: @META@ @PRINT@
*/
int secure[42];
char* p;

void main() {
	{
		int i;
		{
			int i1;
			{
				int letv;
				p[i] = 0;
			}
			p[i] = 1;
		}
		p[i] = 2;
	}
	p[4] = 3;
}

/*@
        \meta::meta \prop,
			\name("writing_1"),
			\targets(main),
			\context(\writing),
		\forall int i21, i, i2;
		\let letv = i21;
		0 <= 42 < i
		&& (i21 == 10 || letv == 20)
		&& i2 == i % i21
		==> \separated(\written, secure + i2);
*/
