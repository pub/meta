/* run.config
   OPT: @META@ -meta-check-ext @PRINT@
*/
struct Bar {
	int a, b;
};
struct Bar foo;
struct Bar* bar;

/*@
	assigns \nothing;
	behavior never:
		assumes p->a == 42;
		assigns p->a \from p->b;
*/
int func(struct Bar* p);

/*@
	requires foo.a != 42;
*/
int main() {
	func(&foo);
	int i = func(&foo);
	return 0;
}

/*@
 \meta::meta \prop, \name("writing_1"), \targets(main), \context(\writing),
   \separated(\written, &foo);
 \meta::meta \prop, \name("reading_2"), \targets(main), \context(\reading),
   \separated(\read, bar);
*/
