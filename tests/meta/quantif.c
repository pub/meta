/* run.config
   OPT: @META@ @PRINT@
*/

/*@ \meta::meta \macro,
	\name(forall_bool),
	\arg_nb(2),
	(\let \param_1 = 0; \param_2) && (\let \param_1 = 1; \param_2);
*/

/*@
  \meta::meta \prop,
		\name(test),
		\targets(\ALL),
		\context(\writing),
		forall_bool(b, b || !b);
*/

void main() {
	int A;
	A = 42;
}
