/* run.config
   OPT: @META@ @PRINT@
*/
void a() {}
void b() {c();}
void c() {a(); b();}
void d() {}
void e() {a();}
void f() {e();}
void main() {c();}

/*@ \meta::meta \prop,
		\name(abc),
		\targets(\callees({main, f})),
		\context(\weak_invariant),
		\false;
*/
