/* run.config
   OPT: @META@ @PRINT@
*/

union U {
    int a, b;
};

struct S {
    int a, b;
};

struct S s;
union U u;

int main() {
    s.a = 42;
    s.b = 42;
    u.a = 42;
    u.b = 42;
}

/*@ \meta::meta \prop,
    \name(ssep),
    \targets(\ALL),
    \context(\writing),
    \separated(\written, &s.a);
*/

/*@ \meta::meta \prop,
    \name(usep),
    \targets(\ALL),
    \context(\writing),
    \separated(\written, &u.a);
*/
