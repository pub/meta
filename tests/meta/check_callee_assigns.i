/* run.config
   OPT: @META@ -meta-check-callee-assigns f,g -meta-no-check-ext @PRINT@
*/

extern int x, y;

extern void h(void);

//@ assigns x \from y;
extern void g(void);

//@ assigns x \from y;
void f(void) {
 x = y;
}

void i(void) {
 x = y;
}

int main() {
  f(); // f is in check-callee-assigns -> generate assertions
  g(); // g is in check-callee-assigns -> generate assertions
  h(); // should not trigger any assertion: no-check-ext
  i(); // should not trigger any assertion: i is not is check-callee-assigns
}

/*@ meta \prop,
  \name(x_is_written),
  \targets(\ALL),
  \context(\writing),
  \tguard(\separated(&x,\written) || \at(x,After) == y);
*/

/*@ meta \prop,
  \name(y_is_read),
  \targets(\ALL),
  \context(\reading),
  \tguard(\separated(&y,\read) || y >= 0);
*/
