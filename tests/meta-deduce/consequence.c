/* run.config
   OPT: @META@ @WP@ -print
*/

int A, B, C, D;

//@ assigns A, B ;
void f1(int v) {
    A = v / 2;
    B = v / 2;
}

//@ assigns A ;
void f2() {
    A = B;
}

//@ assigns C ;
void f3() {
    C = 42;
}

//@ assigns A ;
void f5() {
    A = 42;
}

//@ assigns D, A ;
void f4() {
    D = 24;
    f2(); // A call to f5 would and should invalidate inv_all
}

//@ assigns D, A ;
void f6() {
    f4();
}

/*@ \meta::meta \prop,
        \name(inv_big),
        \targets({f1, f2}),
        \context(\weak_invariant),
        A == B;
*/

/*@ \meta::meta \prop,
        \name(inv_small),
        \targets(f1),
        \context(\weak_invariant),
        \flags(translate:no, proof:deduce),
        A == B;
*/

/*@ \meta::meta \prop,
        \name(no_modif1),
        \targets({f3, f4}),
        \context(\writing),
        \separated(\written, &A);
*/

/*@ \meta::meta \prop,
        \name(no_modif2),
        \targets({f3, f4}),
        \context(\writing),
        \separated(\written, &B);
*/

/*@ \meta::meta \prop,
        \name(inv_all),
        \targets({f1, f2, f3, f4}),
        \context(\weak_invariant),
        \flags(translate:no, proof:deduce),
        A == B;
*/

/*@ \meta::meta \prop,
        \name(false_wit),
        \targets(\ALL),
        \context(\weak_invariant),
        \flags(translate:no, proof:deduce),
        A == C;
*/

/*@ \meta::meta \prop,
        \name(false_wit2),
        \targets(\ALL),
        \context(\weak_invariant),
        \flags(translate:no, proof:deduce),
        \false;
*/
