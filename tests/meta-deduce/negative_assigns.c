/* run.config
   OPT: @META@ @WP@ -print
*/

int A, B;

//@ assigns B;
void f3() {
    B = 42;
}

//@ assigns B;
void f2() {
    B = 12;
    f3();
}

//@ assigns B;
void f1() {
    B = 0;
    f3();
    f2();
}

//@ assigns A, B;
void f0() {
    A = 42;
    f1();
    //@ assert A == 42;
}

/*@ \meta::meta \prop,
        \name(untouched),
        \targets(\callees(f1)),
        \context(\writing),
        \separated(\written, &A);
*/

/*@ \meta::meta \prop,
        \name(nega_incomplete),
        \targets(\diff(\callees(f1), f3)),
        \context(\postcond),
        \flags(proof:deduce, translate:no),
        A == \old(A);
*/

/*@ \meta::meta \prop,
        \name(nega_correct),
        \targets(\callees(f1)),
        \context(\postcond),
        \flags(proof:deduce, translate:yes),
        A == \old(A);
*/
