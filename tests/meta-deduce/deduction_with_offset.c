/* run.config
   OPT: @META@
*/

#define UNTOUCHED_AXIOM(title, tset, loc) \
    \meta::meta \prop, \
    \name(title), \
    \targets(tset), \
    \context(\writing), \
    \flags(proof:axiom, translate:no), \
    \separated(\written, loc);

#define DEDUCED_NEG_ASSIGNS(title, tset, val) \
    \meta::meta \prop, \
    \name(title), \
    \targets(tset), \
    \context(\postcond), \
    \flags(proof:deduce, translate:yes), \
    val == \old(val);

struct S1 {
    int a;
    float b;
};

struct S2 {
    struct S1 s;
    char c;
};

struct S2 g1;
struct S2 g2;

void f1() {}
void f2() {}
void f3() {}
void f4() {}

/*@ UNTOUCHED_AXIOM(u1, \ALL, &g1.c) */

//Should succeed
/*@ DEDUCED_NEG_ASSIGNS(d1, f1, g1.c) */
//Should fail
/*@ DEDUCED_NEG_ASSIGNS(d2, f2, g1) */
//Should fail
/*@ DEDUCED_NEG_ASSIGNS(d3, f3, g1.s) */

/*@ UNTOUCHED_AXIOM(u2, \ALL, &g2) */

//Should succeed
/*@ DEDUCED_NEG_ASSIGNS(d4, \ALL, g2.c) */
//Should succeed
/*@ DEDUCED_NEG_ASSIGNS(d5, \ALL, g2.s.a) */
