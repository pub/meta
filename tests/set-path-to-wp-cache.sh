#!/bin/sh
pwd -P | sed 's:\(.*\)/_build/[^/]*\(/.*$\):\1\2/wp-cache:' > path-to-wp-cache
echo "MetaACSL: using WP cache dir:"
cat path-to-wp-cache
