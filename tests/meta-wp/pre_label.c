/* run.config
   OPT: @META@ @WP@ -print
*/
int x;

//@ assigns \nothing;
void foo() {}

//@ assigns x;
void bar() {
	if(x == 0)
		foo();
	else {
		x = 0;
		foo();
	}
}

//@ assigns x;
void main(int a) {
	x = a;
	bar();
}

/*@ \meta::meta \prop,
		\name("calling_1"),
		\targets(bar),
		\context(\calling),
		\at(x, Pre) != 0 ==> \tguard(\called != foo);
*/
