/* run.config
   OPT: @META@ @WP@ -print
*/

int A = 42;

//@ assigns A;
void f1() {
    A = 42;
}

//@ assigns A;
int f0(void);

//@ assigns A;
int main() {
    A = f0();
    f1();
    //@assert A == 42;
    return A;
}

/*@ \meta::meta \prop,
        \name(axiomatic_requires),
        \targets(f1),
        \context(\weak_invariant),
        \flags(proof:axiom, translate:yes),
        A == 42;
*/
