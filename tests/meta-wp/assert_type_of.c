/* run.config
   OPT: @META@ @WP@ -print
*/

int i1, i2;
float d, e;
enum Foo { ONE, TWO, THREE };
enum Foo f = ONE;

//@ assigns i1 ;
void f1() {
    i1 = 42;
}

//@ assigns d, f, i2 ;
void f2() {
    d = 42.0;
    f = TWO;
    i2 = 43;
}

//@ assigns f ;
void f3() {
    f = THREE;
    f = 42;
}


//Should succeed
/*@ \meta::meta \prop, \name(m1), \targets(f1), \context(\writing),
    \assert_type((int*) \written) ==>
        \at(*\written, After) == 42;
*/

//Should fail once
/*@ \meta::meta \prop, \name(m2), \targets(\ALL), \context(\writing),
    \tguard(\assert_type((int*) \written) ==>
        \at(*\written, After) == 42);
*/

//Should fail once
/*@ \meta::meta \prop, \name(m3), \targets(\ALL), \context(\writing),
    \tguard(\assert_type((enum Foo*) \written) ==>
        (\at(*\written, After) == ONE ||
        \at(*\written, After) == TWO ||
        \at(*\written, After) == THREE));
*/
