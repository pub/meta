/* run.config
   OPT: @META@ @WP@ -wp-warn-key pedantic-assigns=inactive -generated-spec-custom exits:skip,terminates:skip -then-last -eva -print
*/

void called() {
}

void uncalled() {
}

void pointed() {
}

void nowhere() {
}

void main() {
	called();
	void (*ptr)() = &uncalled;
	ptr = &pointed;
	(*ptr)();
}

/*@ \meta::meta \prop, \name("calling_1"), \targets(main), \context(\calling),
			\tguard(\called != uncalled);
*/
/*@ \meta::meta \prop, \name("calling_2"), \targets(main), \context(\calling),
			\tguard(\called != nowhere);
*/
