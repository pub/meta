/* run.config
   OPT: @META@ @WP@ -print
*/

int INSTALL_BYTE = 4;
int ROOT_BYTE = 2;
int NOP;
int checkSum = 1;

struct SemiHidden {
	int NOT_HIDDEN;
	int HIDDEN;
};

struct SemiHidden STATE;

/*@
	requires \valid(a);
	requires \valid(b);
  assigns *a;
 */
void RW(int* a, int* b) {
	*a = *b;
	return;
}

/*@
	requires \valid(a);
  assigns ROOT_BYTE;
*/
int compute(int* a) {
	int* b = &ROOT_BYTE;
	*b = STATE.NOT_HIDDEN;
	int test = 'b';
	test = 'c';
	b = &INSTALL_BYTE;
	b = &test;
	*b = 'd';
	return *a / 2;
}
//@ assigns INSTALL_BYTE, STATE.HIDDEN, ROOT_BYTE, checkSum ;
int main() {
	if(checkSum) {
		int* T[] = {&INSTALL_BYTE, &ROOT_BYTE, &STATE.HIDDEN};
		RW(&INSTALL_BYTE, &ROOT_BYTE);
		int* a = &ROOT_BYTE;
		a = &INSTALL_BYTE;
		**(T + 2) = 42;
		compute(&(STATE.NOT_HIDDEN));
	}
	else checkSum = 2;
	return 0;
}

/*@
	// All the following should be true after a pass of WP (with model raw) and EVA
	\meta::meta \prop, \name("reading_1"), \targets(compute), \context(\reading),
			\separated(\read, &STATE.HIDDEN);
        \meta::meta \prop, \name("reading_2"), \targets(\ALL), \context(\reading),
			\separated(\read, &NOP);
        \meta::meta \prop, \name("writing_3"), \targets({compute, main}), \context(\writing),
			\separated(\written, &INSTALL_BYTE);
        \meta::meta \prop, \name("writing_4"), \targets(\diff(\ALL, compute)), \context(\writing),
			\separated(\written, &ROOT_BYTE);
        \meta::meta \prop, \name("strong_invariant_5"), \targets(\ALL), \context(\strong_invariant),
			checkSum == 1;
        \meta::meta \prop, \name("writing_6"), \targets(main), \context(\writing),
			!checkSum ==> \separated(\written, &STATE.HIDDEN);
*/
