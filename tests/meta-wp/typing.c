/* run.config
   OPT: @META@ @WP@ -wp-print -wp-no-let
*/
#include <stdlib.h>
#include <string.h>

struct S {
	unsigned a, b, c, d, f, e, g, h, i;
};

struct S* t;

/*@
	requires \exists int i; s == t + i;
  assigns s->a;
*/
void f(struct S* s) {
	s->a = 42;
}

/*@ \meta::meta \prop, \name(test), \targets(\ALL), \context(\writing),
		\forall unsigned i; \separated(\written, &t[i].b);
*/
