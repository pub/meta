/* run.config
   OPT: @META@ @WP@ -print
*/

int foo = 1;

//@ assigns foo ;
int f() {
  //@ loop assigns i, foo;
	for(int i = 0 ; i < 150 ; ++i) {
		//@ \meta::imeta lenient;
		{
			foo += foo;
			if(foo > 1000) foo = 1;
		}
	}
	return foo;
}

//@ assigns foo ;
int main() {
    foo = -5;
    f();
    return 0;
}

/*@ \meta::meta
		\prop,
			\name("strong_invariant_1"),
			\targets(f),
			\context(\strong_invariant),
			foo > 0;
*/
