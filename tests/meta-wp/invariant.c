/* run.config
   OPT: @META@ @WP@ -print
*/

#include <assert.h>

unsigned A = 2, B;

//@ assigns \nothing;
void requiresInv() {
	assert(A % 2 == 0);
}
//@ assigns A;
void breaksInv() {
	A += 1;
}

//@ assigns \nothing;
void useless() {
}

//@ assigns A;
void maintainsInv() {
	if(A < 42)
		A += 2;
	else A -= 4;
	useless();
}

//@ assigns B;
void cond() {
    if(B != 42){
        B *= 2;
    }
}

//@ assigns A, B;
void main() {
    cond();
	maintainsInv();
	requiresInv();

	//@ \meta::imeta lenient;
	{
		++A;
		if(A < 100)
			A *= 2;
		else A = 2;
	}

	breaksInv();
	requiresInv();
}

/*@
        \meta::meta \prop,
			\name("strong_invariant_1"),
			\targets(main),
			\context(\strong_invariant),
		A % 2 == 0;

        \meta::meta \prop,
			\name("invariant_2"),
			\targets(\diff(\ALL, {main, useless, cond})),
			\context(\weak_invariant),
		A % 2 == 0;

    \meta::meta \prop,
            \name(conditional_true),
            \targets(cond),
            \context(\conditional_invariant),
        B == 42;

    \meta::meta \prop,
            \name(conditional_false),
            \targets(cond),
            \context(\conditional_invariant),
        B == 43;
*/
