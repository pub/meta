/* run.config
   OPT: @META@ @WP@ -print
*/

int a = 42;
int forbidden[] = {42, 2713};

//@ assigns a ;
int main() {
	a = 1;
	a = 42;
	a = 41;
	a = 100;
	return 0;
}

/*@ \meta::meta \prop,
		\name("writing_1"),
		\targets(\ALL),
		\context(\writing),
	(\exists unsigned i;  0 <= i < 2 && a == forbidden[i])
	==> \separated(\written, &a);
*/

