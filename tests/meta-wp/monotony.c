/* run.config
   OPT: @META@ @WP@ -print
*/

int a = 0;
//@ assigns a;
int main() {
	a = 1;
	a = 42;
	a = 41;
	a = 100;
	return 0;
}

/*@ \meta::meta \prop,
		\name("writing_1"),
		\targets(\ALL),
		\context(\writing),
	\at(a, Before) < \at(a, After) || \separated(\written, &a);
*/
