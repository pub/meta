/* run.config
   OPT: @META@ @WP@ -print
   OPT: @META@ -meta-checks @WP@ -print
*/

int C1, C2;
int A, B, C, D;

//@ assigns A, B, C, D;
void f1() {
    //@ \meta::imeta lenient;
    {
        A = C1;
        B = C2;
    }
    //@ \meta::imeta lenient;
    {
        C = 4;
        D = 2;
    }
}

//@ assigns \nothing ;
void f2() {
}

//@ assigns A;
void f3() {
  A = C1;
}

//@ assigns C;
void f4() {
  C = C2;
}

/*@ \meta::meta \prop,
        \name(test),
        \targets(\ALL),
        \context(\strong_invariant),
        \flags(proof:axiom),
        A == B;
*/

/*@ \meta::meta \prop,
        \name(test2),
        \targets(\ALL),
        \context(\strong_invariant),
        C == D;
*/

/*@ \meta::meta \prop,
        \name(empty),
        \targets(f2),
        \context(\writing),
        \flags(translate:false, proof:axiom),
        this_will_never_be_instantiated;
*/

/*@ \meta::meta \prop,
    \name(always_check),
    \targets(f3),
    \context(\writing),
    \flags(translate:check),
    A == B;
*/

/*@ \meta::meta \prop,
    \name(always_assert),
    \targets(f4),
    \context(\writing),
    \flags(translate:assert),
    C==D;
*/
