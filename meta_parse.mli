(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

open Cil_types
open Meta_utils

type context =
  | Weak_invariant
  | Strong_invariant
  | Calling
  | Writing
  | Reading
  | Postcond
  | Precond
  | Conditional_invariant

type target =
  | TgAll
  | TgSet of StrSet.t
  | TgUnion of target list
  | TgInter of target list
  | TgDiff of target * target
  | TgCallees of target
  | TgCallers of target
  | TgFile of string

type mp_proof_method =
  | MmLocal
  | MmDeduction
  | MmAxiom

type mp_translation =
  | MtCheck
  | MtAssert
  | MtDefault
  | MtNone

type replaced_kind =
  | RepVariable of term (** replace variable by term *)
  | RepApp of (string * term) list (** replace application with arg by associated term *)

type metaproperty = {
  mp_name : string;
  mp_target : target;
  mp_context : context;
  mp_property : Kernel_function.t -> (string * replaced_kind) list -> predicate;
  mp_proof_method : mp_proof_method;
  mp_translation : mp_translation;
  mp_loc : Cil_types.location
}

val register_parsing : unit -> unit

(** The list of meta-properties in the file, in the same order *)
val metaproperties : unit -> metaproperty list
