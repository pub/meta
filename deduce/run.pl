#!/usr/bin/env swipl
:- encoding(utf8). % needed because of the accents in the headers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                        %
%  This file is part of the Frama-C's MetACSL plug-in.                   %
%                                                                        %
%  Copyright (C) 2018-2025                                               %
%    CEA (Commissariat à l'énergie atomique et aux énergies              %
%         alternatives)                                                  %
%                                                                        %
%  you can redistribute it and/or modify it under the terms of the GNU   %
%  Lesser General Public License as published by the Free Software       %
%  Foundation, version 2.1.                                              %
%                                                                        %
%  It is distributed in the hope that it will be useful,                 %
%  but WITHOUT ANY WARRANTY; without even the implied warranty of        %
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         %
%  GNU Lesser General Public License for more details.                   %
%                                                                        %
%  See the GNU Lesser General Public License version 2.1                 %
%  for more details (enclosed in the file LICENSE)                       %
%                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- initialization main.

prove(G, Timeout, Res) :-
    atom_number(Timeout, T),
    RT is T * 1000,
    setlog(G, RT, _, Res) ; Res = no.

usage :-
    write("Usage : run.pl [prove/repl] GOAL_FILE TIMEOUT"), nl, halt.

main :-
    current_prolog_flag(argv, [ Action | [Goal | [Timeout | _ ]]]) -> (
        init_slog(Goal),
        Action = prove -> (
            prove(go, Timeout, Res),
            write(Res),
            (Res = success, halt(0)) ; halt(1)
        ) ; ( Action = repl ->
            setlog
        ) ; usage
    ) ; usage.

init_slog(Goal) :-
    consult('setlog.pl'),
    consult_lib,
    setlog_rw_rules,
    setlog(strategy(ordered)),
    setlog(add_lib('model.slog',[encoding(utf8)])),
    setlog_consult(Goal).
