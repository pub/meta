(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

(** Various utilities. *)

(** {1 shortcuts} *)

module StrSet: Datatype.Set with type elt = string

module Str_Hashtbl: Datatype.Hashtbl with type key = string

module Stmt_Hashtbl: Datatype.Hashtbl with type key = Cil_types.stmt

module Fundec_Hashtbl: Datatype.Hashtbl with type key = Cil_types.fundec

module Fundec_Set: Datatype.Set with type elt = Cil_types.fundec

(** {1 Hashtbl utilities} *)

(** [find_hash_list find_opt tbl key] tries to find [key] in [tbl] using
    [find_opt] and returns the empty list if it is not found.
*)
val find_hash_list: ('a -> 'b -> 'c list option) -> 'a -> 'b -> 'c list

(** [add_to_hash_list (find_opt, replace) tbl key v] adds [v] to the list
    of elements associated to [key] in [tbl], using [find_opt] and
    [replace] functions. *)
val add_to_hash_list:
  ('a -> 'b -> 'c list option) * ('a -> 'b -> 'c list -> unit) ->
  'a -> 'b -> 'c -> unit
