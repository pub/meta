# Version 0.8

- compatibility with Frama-C 30.0 Zinc

# Version 0.7

- compatibility with Frama-C 29.0 Copper

# Version 0.6

- compatibility with Frama-C 28.x Nickel
- `-meta-check-callee-assigns` can now also be given declared functions

# Version 0.5

- compatibility with Frama-C 27.x Cobalt
- added `-meta-check-callee-assigns` option (contributed by Thales Research & Technology)
- added `\lhost_written` and `\lhost_read` meta-variables

# Version 0.4

- compatibility with Frama-C 26.0 Iron
- ensure Wookey case study can be handled (with non-free ACSL-importer plug-in)

# Version 0.3

- compatibility with Frama-C 25.0 Manganese

# Version 0.2

- add warning category `unknown-func` which aborts by default
- add `\func` meta-variables in all contexts
- remove unused `-meta-eacsl` option
- more simplification of trivial instances
- add `\called_arg` family of meta-variables in `\calling` context

# Version 0.1: first public release
