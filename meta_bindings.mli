(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

open Cil_types
open Logic_typing
open Meta_utils

val types : Cil_types.logic_type Str_Hashtbl.t
val parse_bound : typing_context -> location ->
  logic_var Str_Hashtbl.t -> Logic_ptree.lexpr -> term
val parse_bind : typing_context -> location -> string -> string ->
  acsl_extension_kind
val after_parse : typing_context -> predicate ->
  logic_var Str_Hashtbl.t -> predicate
val add_ghost_code : Meta_options.meta_flags -> unit

val process_imeta: Acsl_extension.extension_visitor
