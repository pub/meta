(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's MetACSL plug-in.                   *)
(*                                                                        *)
(*  Copyright (C) 2018-2025                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file LICENSE)                       *)
(*                                                                        *)
(**************************************************************************)

(** Command line options and plugin registration. *)

module Self: Plugin.S

(** value of [-meta] *)
module Enabled: Parameter_sig.Bool

(** value of [-meta-check-ext] *)
module Check_External: Parameter_sig.Bool

(** value of [-meta-check-asgn] *)
module Check_Callee_Assigns: Parameter_sig.Kernel_function_set

(** value of [-meta-set] *)
module Targets: Parameter_sig.String_set

(** value of [-meta-simpl] *)
module Simpl: Parameter_sig.Bool

(** value of [-meta-number-assertions] *)
module Number_assertions: Parameter_sig.Bool

(** value of [-meta-add-prefix] *)
module Prefix_meta: Parameter_sig.Bool

(** value of [-meta-list-targets] *)
module List_targets: Parameter_sig.Bool

(** value of [-meta-keep-proof-files] *)
module Keep_proof_files: Parameter_sig.Bool

(** value of [-meta-static-bindings] *)
module Static_bindings: Parameter_sig.Int

(** value of [-meta-separate-annots] *)
module Separate_annots: Parameter_sig.Bool

(** value of [-meta-asserts] *)
module Default_to_assert: Parameter_sig.Bool

val unknown_func_wkey: Self.warn_category

(** record with all the options as set at the start
    of the analysis. *)
type meta_flags =  {
  check_external : bool;
  check_callee_assigns: Kernel_function.Set.t;
  simpl : bool;
  static_bindings : int option;
  target_set : Meta_utils.StrSet.t option;
  number_assertions : bool;
  prefix_meta : bool;
  list_targets : bool;
  keep_proof_files : bool;
  default_to_assert : bool;
}
